FROM node:alpine
LABEL authors="arkadiusz.tulodziecki@mobica.com"

RUN apk update && apk add --no-cache --virtual .gyp python3-dev py3-pip make g++ net-tools

RUN npm install -g cobertura-merge

RUN pip3 install pycobertura
